class Cell {
    constructor(scene, x, y, type, corners) {
        this.x = x;
        this.y = y;

        this.scene = scene;
        this.container = scene.add.container(
            playground.countXCoord(x, y),
            playground.countYCoord(x, y)
        );
        this.container.depth = (x + y) * 1000 - x * 10;

        this.highlighters = [];
        this.tint = 0xaaaaaa;

        this.walls = {
            top_left: { empty: true, sprite: undefined, index: 0 },
            top_right: { empty: true, sprite: undefined, index: 1 },
            bottom_right: { empty: true, sprite: undefined, index: 2 },
            bottom_left: { empty: true, sprite: undefined, index: 3 },
        };
        this.corners = {
            top: { empty: true, sprite: undefined, index: 0 },
            right: { empty: true, sprite: undefined, index: 1 },
            bottom: { empty: true, sprite: undefined, index: 2 },
            left: { empty: true, sprite: undefined, index: 3 },
        };

        this.sprite = this.scene.add.sprite(0, 0, "floor_tiles", "base.png");
        this.sprite.tint = this.tint;
        this.container.add(this.sprite);

        const bitCorners = corners.toString(2).split('').reverse().map(b => +b);

        if (bitCorners[3]) {
            this.corners.top.empty = false;
            this.corners.top.sprite = this.setWallSprite(
                0,
                -87,
                "walls",
                "base_corner.png",
                0
            );
            if (playground.findCell(x - 1, y - 1))
                this.corners.top.sprite.alpha = 0.6;
        }

        if (bitCorners[2]) {
            this.corners.right.empty = false;
            this.corners.right.sprite = this.setWallSprite(
                52,
                -48,
                "walls",
                "base_corner.png",
                5
            );
            if (
                playground.findCell(x, y - 1) ||
                playground.findCell(x - 1, y - 1)
            )
                this.corners.right.sprite.alpha = 0.6;
        }

        if (bitCorners[1]) {
            this.corners.bottom.empty = false;
            this.corners.bottom.sprite = this.setWallSprite(
                0,
                -9,
                "walls",
                "base_corner.png",
                9
            );
            this.corners.bottom.sprite.alpha = 0.6;
        }

        if (bitCorners[0]) {
            this.corners.left.empty = false;
            this.corners.left.sprite = this.setWallSprite(
                -52,
                -48,
                "walls",
                "base_corner.png",
                5
            );
            if (
                playground.findCell(x - 1, y) ||
                playground.findCell(x - 1, y - 1)
            )
                this.corners.left.sprite.alpha = 0.6;
        }

        if (type == 1 || type == 3) {
            this.walls.top_left.empty = false;
            this.walls.top_left.sprite = this.setWallSprite(
                -32,
                -63,
                "walls",
                "base_left.png",
                2
            );
            if (
                playground.findCell(x - 1, y) ||
                playground.findCell(x - 1, y - 1)
            )
                this.walls.top_left.sprite.alpha = 0.6;
        }

        if (type == 2 || type == 3) {
            this.walls.top_right.empty = false;
            this.walls.top_right.sprite = this.setWallSprite(
                32,
                -63,
                "walls",
                "base_right.png",
                2
            );
            if (
                playground.findCell(x, y - 1) ||
                playground.findCell(x - 1, y - 1)
            )
                this.walls.top_right.sprite.alpha = 0.6;
        }

        this.gameObject = null;

        this.container.sort("depth");
        this.highlight(false)
        playground.addCell(x, y, this);
    }

    set color(tint){
        this.tint = tint;
        this.container.list.forEach(s => {
            if (!this.gameObject || s !== this.gameObject.sprite) s.setTint(tint); 
        });
    }

    /**
     * функция создания спрайта стен клетки
     * @param {number} x - координата спрайта в контейнере
     * @param {number} y - координата спрайта в контейнере
     * @param {string} texture - ключевое имя текстуры спрайта
     * @param {string} frame - имя фрейма в атласе текстуры спрайта
     * @returns {Phaser.GameObject.Sprite}
     */
    setWallSprite(x, y, texture, frame, depth) {
        let sprite = this.scene.add.sprite(x, y, texture, frame);
        sprite.displayWidth = 64;
        sprite.displayHeight = 125;
        sprite.tint = this.tint;
        sprite.depth = depth;
        this.container.add(sprite);

        return sprite;
    }

    /**
     * функция подсветки клетки
     * @param {bool} state - true - для включения подсветки, false - для выключения
     * @param {object} initiator - объект-инициатор подсветки клетки
     * @param {number} speed - скорость изменения подсветки (опционально)
     */
    highlight(state, initiator, speed) {
        if (this.tween) this.scene.tweens.remove(this.tween);

        if (state) this.highlighters.push(initiator);
        else
            this.highlighters = this.highlighters.filter(
                (x) => x !== initiator
            );

        const newTint = this.highlighters.length ? 0xffffff : 0xaaaaaa;
        if (this.tint !== newTint) {
            if (speed) {
                this.tween = this.scene.tweens.addCounter({
                    from: playground.hexColorToRGB(this.tint)[0],
                    to: playground.hexColorToRGB(newTint)[0],
                    duration: 1000 / speed,
                    onUpdate: function (tween)
                    {
                        const newValue = Math.floor(tween.getValue());
                        this.color = playground.rgbColorToHEX([newValue, newValue, newValue]);
                    },
                    onUpdateScope: this
                });
            }
            else this.color = newTint;
        }
        if (this.gameObject && this.gameObject !== initiator) this.gameObject.fade(state, speed);
    }

    addObject(obj){
        obj.cell = this;
        this.gameObject = obj;
        this.container.add(obj.sprite);
        this.container.sort("depth");
    }

    removeObject(obj){
        obj.cell = null;
        this.gameObject = null;
        this.container.remove(obj.sprite);
    }
}
