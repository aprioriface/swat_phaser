/**
 * фуекция-конструктор структуры анимаций
 * @param {*} animations - массив объектов, описывающих анимации
 * {
        name: "walk_tl",
        frameCount: 25,
        key: "unit_walk",
        frame: "swat_walk_v$$_##.png",
        version: "03",
        duration: 1,
        visible: false,
        startframe: 0
    } 
 */

function createAnimationJSON(animations){
    let json = []

    animations.forEach(e => {
        let jsonElement = {
            "key": e.name,
            "type": "frame",
            "frames": [
            ],
            "frameRate": 24,
            "duration": 3.4285714285714284,
            "skipMissedFrames": true,
            "delay": 0,
            "repeat": -1,
            "repeatDelay": 0,
            "yoyo": false,
            "showOnStart": false,
            "hideOnComplete": false
        }
    
        for (let i = e.startFrame; i < e.frameCount; i++){
            jsonElement.frames.push({
               key: e.key, 
               frame: e.frame.replace("##", i < 10 ? `0${i}` : i).replace("$$", e.version), 
               duration: e.duration, 
               visible: e.visible
            })   
        }

        json.push(jsonElement)
    });
    
    document.querySelector("textarea").value = JSON.stringify(json, null, ' ')
}