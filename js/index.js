var game = new Phaser.Game({
    type: Phaser.AUTO,
    width: window.innerWidth,
    height: window.innerHeight,
    scene: {
        preload: preload,
        create: create,
        update: update,
    },
});
var playground;

function preload() {
    playground = new Playground(this, game.config.width / 2, 200);

    this.load.animation("unit_anims", "assets/anims/unit.json");

    this.load.atlas(
        "floor_tiles",
        "assets/sprites/images/floor_tiles.png",
        "assets/sprites/jsons/floor_tiles.json"
    );
    this.load.atlas(
        "walls",
        "assets/sprites/images/walls.png",
        "assets/sprites/jsons/walls.json"
    );
    this.load.atlas(
        "unit_idle",
        "assets/sprites/images/unit_idle.png",
        "assets/sprites/jsons/unit_idle.json"
    );
    this.load.atlas(
        "unit_walk",
        "assets/sprites/images/unit_walk.png",
        "assets/sprites/jsons/unit_walk.json"
    );
    this.load.atlas(
        "unit_sit",
        "assets/sprites/images/unit_sit.png",
        "assets/sprites/jsons/unit_sit.json"
    );
    this.load.atlas(
        "unit_shoot",
        "assets/sprites/images/unit_shoot.png",
        "assets/sprites/jsons/unit_shoot.json"
    );
}

function create() {
    new Team("blue", 0xCCCCFF);
    new Team("red", 0xFFCCCC);
    new Team("green", 0xCCFFCC);

    playground.fieldMap.map((o, j) =>
        o.map((x, i) => new Cell(this, i, j, x[0], x[1]))
    );
    playground.fillWallArrays();

    new Unit(this, 1, 0, 'idle', 'bl', "blue");
    new Unit(this, 2, 0, 'sit', 'bl', "blue");
    new Unit(this, 0, 5, 'idle', 'bl', "red");
    new Unit(this, 1, 3, 'sit', 'tr', "green");

    playground.container.sort("depth");
}

function update() {}
