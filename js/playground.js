class Playground {
    constructor(scene, x, y) {
        this.fieldMap = [
            [
                [3, 0b1000],
                [2, 0b0010],
                [2, 0b0100],
            ],
            [
                [1, 0],
                [0, 0],
                [0, 0],
            ],
            [
                [1, 0],
                [0, 0],
                [0, 0],
            ],
            [
                [3, 0b1000],
                [2, 0b0100],
                [0, 0],
            ],
            [
                [1, 0b0010],
                [0, 0b0010],
                [0, 0],
            ],
            [
                [1, 0b0001],
                [0, 0],
                [0, 0],
            ],
        ];
        this.player = {
            team: undefined
        };
        this.teams = {};
        this.cells = [];
        this.units = [];
        this.scene = scene;
        this.container = scene.add.container(x, y);
    }

    addTeam(team){
        this.teams[team.name] = team;
        if (!this.player.team) this.player.team = team;
    }

    /**
     * функция добавления клетки поля в матрицу клеток
     * @param {number} x - координата клетки в матрице
     * @param {number} y - координата клетки в матрице
     * @param {Cell} data - данные клетки
     */
    addCell(x, y, cell) {
        if (!this.cells[y]) this.cells[y] = [];
        this.cells[y][x] = cell;
        this.container.add(cell.container);
    }

    /**
     * функция поиска данных клетки в матрице клеток
     * возвращает экземпляр класса Cell, если клетка найдена
     * и undefined - если нет
     * @param {number} x - координата клетки в матрице
     * @param {number} y - координата клетки в матрице
     */
    findCell(x, y) {
        try {
            return this.cells[y][x];
        } catch {
            return undefined;
        }
    }

    addUnit(unit){
        this.units.push(unit);

        const targetCell = this.findCell(unit.x, unit.y);
        if (targetCell){
            targetCell.addObject(unit);        
        }
    }

    findUnit(x, y, team) {
        return this.units.find(u => u.x === x && u.y === y && (!team || u.team === team));
    }
    
    findUnitsByTeam(team) {
        return this.units.filter(u => u.team === team);
    }

    /**
     * функция вычисления X координаты спрайта объекта на полотне
     * @param {number} x - координата объекта в матрице
     * @param {number} y - координата объекта в матрице
     * @param {object} obj - экземпляр класса объекта, для которого вычисляется координата
     */
    countXCoord(x, y) {
        return 51 * (x - y);
    }

    /**
     * функция вычисления Y координаты спрайта объекта на полотне
     * @param {number} x - координата объекта в матрице
     * @param {number} y - координата объекта в матрице
     * @param {object} obj - экземпляр класса объекта, для которого вычисляется координата
     */
    countYCoord(x, y) {
        return 38 * (x + y);
    }

    /**
     * функция вычисляющая состояние стен клеток игрового поля
     */
    fillWallArrays() {
        this.cells.forEach((r) => {
            r.forEach((c) => {
                let targetCell = this.findCell(c.x + 1, c.y);
                c.walls.bottom_right.empty = targetCell
                    ? targetCell.walls.top_left.empty
                    : false;
                targetCell = this.findCell(c.x, c.y + 1);
                c.walls.bottom_left.empty = targetCell
                    ? targetCell.walls.top_right.empty
                    : false;
            });
        });
    }

    /**
     * функция, раскладывающая hex значение цвета в массив [R,G,B]
     * @param {number} color - значение цвета в виде числа
     */
    hexColorToRGB(color) {
        return [
            parseInt(color.toString(16).substring(0, 2), 16),
            parseInt(color.toString(16).substring(2, 4), 16),
            parseInt(color.toString(16).substring(4, 6), 16),
        ];
    }

    /**
     * функция, собирающая hex значение цвета из массива [R,G,B]
     * @param {Array[3]} color - значение цвета в виде массива [R,G,B]
     */
    rgbColorToHEX(color) {
        return parseInt(color.map((x) => x.toString(16)).join(""), 16);
    }

    /**
     * функция, вычисляющая новое значение цвета с заданным шагом
     * @param {Array[3]} color1 - текущее значение цвета в виде массива [R,G,B]
     * @param {Array[3]} color2 - целевое значение цвета в виде массива [R,G,B]
     * @param {number} step - шаг изменения цвета
     * @returns {Array[3]} - итоговое значение цвета в виде массива [R,G,B] 
     */
    mixRGBColors(color1, color2, step) {
        const newColor = [];
        for (let i = 0; i < 3; i++) {
            if (color1[i] > color2[i]) {
                newColor[i] = color1[i] - step;
                newColor[i] = newColor[i] < color2[i] ? color2[i] : newColor[i];
            } else {
                newColor[i] = color1[i] + step;
                newColor[i] = newColor[i] > color2[i] ? color2[i] : newColor[i];
            }
        }
        return newColor;
    }

    /**
     * функция, вычисляющая новое значение цвета с заданным шагом
     * @param {number} color1 - текущее значение цвета в виде числа 
     * @param {number} color2 - целевое значение цвета в виде числа 
     * @param {number} step - шаг изменения цвета
     * @returns {number} - итоговое значение цвета в виде числа 
     */
    mixHEXColors(color1, color2, step) {
        return this.rgbColorToHEX(
            this.mixRGBColors(
                this.hexColorToRGB(color1),
                this.hexColorToRGB(color2),
                step
            )
        )
    }
}
