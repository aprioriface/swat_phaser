class Team{
    constructor(name, tint, points){
        this.name = name;
        this.tint = tint;
        this.points = points ? points : 0;

        playground.addTeam(this);
    }
}