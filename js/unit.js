class Unit {
    constructor(scene, x, y, state, direction, team, speed) {
        this.team = playground.teams[team];
        this.scene = scene;

        this.state = state;
        this.direction = direction;
        this.x = x;
        this.y = y;

        this.visibility = {
            distance: 3,
            straight: false,
            cells: [],
        };

        this.frames = new UnitAnimation(this);
        this.sprite = this.scene.add.sprite(0, -10, 'unit_anim');
        this.sprite.tint = this.team.tint;

        //this.sprite.animationSpeed = speed ? speed : 0.25;
        this.sprite.scaleX = 0.5;
        this.sprite.scaleY = 0.5;
        this.sprite.depth = 5;
        this.sprite.play(`${state}_${direction}`);

        this.cell = null;

        if (this.team === playground.player.team){
            this.fillVisibleCells();
            this.highlightVisibleCells(true, 0);
        }
        else this.fade(false);

        playground.addUnit(this);
    }

    /**
     * функция вычисления видимых клеток юнита
     * @param {number} x - координата клетки юнита (опционально)
     * @param {number} y - координата клетки юнита (опционально)
     * @param {string} direction - направление взгляда юнита (опционально)
     */
    fillVisibleCells(x, y, direction) {
        x = x || this.x;
        y = y || this.y;
        direction = direction || this.direction;

        const visibleCells = [];
        let lastMainCell;

        for (let i = 0; i <= this.visibility.distance; i++) {
            let visibleX = x;
            let visibleY = y;
            let shiftX = 0;
            let shiftY = 0;
            let helpCells = [];
            let problemWall = "";
            let testWalls = [];

            switch (direction) {
                case "bl":
                    visibleY += i;
                    shiftX = 1;
                    problemWall = "top_right";
                    helpCells = [
                        [
                            { x: -1, y: 0 },
                            { x: -1, y: -1 },
                        ],
                        [
                            { x: 1, y: 0 },
                            { x: 1, y: -1 },
                        ],
                    ];
                    testWalls = ["bottom_right", "top_left"];
                    break;
                case "br":
                    visibleX += i;
                    shiftY = 1;
                    problemWall = "top_left";
                    helpCells = [
                        [
                            { x: 0, y: -1 },
                            { x: -1, y: -1 },
                        ],
                        [
                            { x: 0, y: 1 },
                            { x: -1, y: 1 },
                        ],
                    ];
                    testWalls = ["bottom_left", "top_right"];
                    break;
                case "tr":
                    visibleY -= i;
                    shiftX = 1;
                    problemWall = "bottom_left";
                    helpCells = [
                        [
                            { x: -1, y: 0 },
                            { x: -1, y: 1 },
                        ],
                        [
                            { x: 1, y: 0 },
                            { x: 1, y: 1 },
                        ],
                    ];
                    testWalls = ["bottom_right", "top_left"];
                    break;
                case "tl":
                    visibleX -= i;
                    shiftY = 1;
                    problemWall = "bottom_right";
                    helpCells = [
                        [
                            { x: 0, y: -1 },
                            { x: 1, y: -1 },
                        ],
                        [
                            { x: 0, y: 1 },
                            { x: 1, y: 1 },
                        ],
                    ];
                    testWalls = ["bottom_left", "top_right"];
                    break;
            }

            let helpCell1, helpCell2;
            let targetCell = playground.findCell(visibleX, visibleY);
            if (
                i === 0 ||
                (targetCell &&
                    targetCell.walls[problemWall].empty &&
                    visibleCells.find((x) => x === lastMainCell))
                )
                visibleCells.push(targetCell);

            lastMainCell = targetCell;

            if (!this.visibility.straight) {
                for (let j = i; j > 0; j--) {
                    targetCell = playground.findCell(
                        visibleX + shiftX * j,
                        visibleY + shiftY * j
                    );
                    if (targetCell) {
                        helpCell1 = playground.findCell(
                            targetCell.x + helpCells[0][0].x,
                            targetCell.y + helpCells[0][0].y
                        );
                        helpCell2 = playground.findCell(
                            targetCell.x + helpCells[0][1].x,
                            targetCell.y + helpCells[0][1].y
                        );

                        if (j === i) {
                            if (
                                visibleCells.find((x) => x === helpCell2) &&
                                !(
                                    !targetCell.walls[problemWall].empty &&
                                    !helpCell1.walls[problemWall].empty
                                ) &&
                                !(
                                    !helpCell1.walls[testWalls[0]].empty &&
                                    !helpCell2.walls[testWalls[0]].empty
                                ) &&
                                !(
                                    !helpCell2.walls[testWalls[0]].empty &&
                                    !helpCell1.walls[problemWall].empty
                                ) &&
                                !(
                                    !targetCell.walls[problemWall].empty &&
                                    !targetCell.walls[testWalls[1]].empty
                                )
                            )
                                visibleCells.push(targetCell);
                        } else {
                            if (
                                visibleCells.find((x) => x === helpCell2) &&
                                targetCell.walls[problemWall].empty &&
                                helpCell2.walls[testWalls[0]].empty
                            )
                                visibleCells.push(targetCell);
                        }
                    }

                    targetCell = playground.findCell(
                        visibleX - shiftX * j,
                        visibleY - shiftY * j
                    );
                    if (targetCell) {
                        helpCell1 = playground.findCell(
                            targetCell.x + helpCells[1][0].x,
                            targetCell.y + helpCells[1][0].y
                        );
                        helpCell2 = playground.findCell(
                            targetCell.x + helpCells[1][1].x,
                            targetCell.y + helpCells[1][1].y
                        );

                        if (j === i) {
                            if (
                                visibleCells.find((x) => x === helpCell2) &&
                                !(
                                    !targetCell.walls[problemWall].empty &&
                                    !helpCell1.walls[problemWall].empty
                                ) &&
                                !(
                                    !helpCell1.walls[testWalls[1]].empty &&
                                    !helpCell2.walls[testWalls[1]].empty
                                ) &&
                                !(
                                    !helpCell2.walls[testWalls[1]].empty &&
                                    !helpCell1.walls[problemWall].empty
                                ) &&
                                !(
                                    !targetCell.walls[problemWall].empty &&
                                    !targetCell.walls[testWalls[0]].empty
                                )
                            )
                                visibleCells.push(targetCell);
                        } else {
                            if (
                                visibleCells.find((x) => x === helpCell2) &&
                                targetCell.walls[problemWall].empty &&
                                helpCell2.walls[testWalls[1]].empty
                            )
                                visibleCells.push(targetCell);
                        }
                    }
                }
            }
        }

        this.visibility.cells = visibleCells;
    }

    /**
     * функция подсветки видимых клеток юнита
     * @param {bool} state - true - включает подсветку, false - выключает
     * @param {number} speed - скорость изменения подсветки (опционально)
     */
    highlightVisibleCells(state, speed) {
        this.visibility.cells.forEach((c) => {
            c.highlight(state, this, speed);
        });
    }

    /**
     * функция, изменяющая текстуру спрайта
     * @param {string} state - тип анимации (idle, shoot, sit)
     * @param {string} direction - направление анимации (tl, tr, bl, br)
     * @param {number} speed - скорость анимации (опционально)
     * @param {bool} loop - флаг цикличности анимации (опционально)
     */
    resprite(state, direction, speed, loop) {
        if (this.direction !== direction || this.state !== state) {
            //if (speed) this.sprite.animationSpeed = speed;
            //this.sprite.loop = loop ? true : false;
            //if (loop)
            //    this.sprite.gotoAndPlay(
            //        Math.floor(Math.random() * this.sprite.textures.length)
            //    );
            this.sprite.play(`${state}_${direction}`);

            //this.texture = newTexture;
            this.state = state;
            this.direction = direction;
        }
    }

    replace(x, y, direction){
        this.x = x;
        this.y = y;
        
        this.cell.removeObject(this);
        playground.findCell(x,y).addObject(this);

        switch(direction){
            case 'bl':
                this.sprite.setPosition(51, -48);
                this.sprite.setDepth(6);
                break;
            case 'tl':
                this.sprite.setPosition(51, 28);
                this.sprite.setDepth(3);
                break;
            case 'tr':
                this.sprite.setPosition(-51, 28);
                this.sprite.setDepth(3);
                break;
            case 'br':
                this.sprite.setPosition(-51, -48);
                this.sprite.setDepth(6);
                break;
        }
    }

    revision({ x, y, direction, straight, speed }){
        x = (x === undefined) ? this.x : x
        y = (y === undefined) ? this.y : y
        direction = (direction === undefined) ? this.direction : direction
        straight = (straight === undefined) ? this.visibility.straight : straight
        speed = (speed === undefined) ? 1 : speed

        this.highlightVisibleCells(false, speed);
        this.visibility.straight = straight;
        this.fillVisibleCells(x, y, direction);
        this.highlightVisibleCells(true, speed);
    }

    fade(state, speed){
        if (this.tween) this.scene.tweens.remove(this.tween);

        if (speed) {
            this.tween = this.scene.tweens.addCounter({
                from: this.sprite.alpha,
                to: state ? 1 : 0,
                duration: 1000 / speed,
                onUpdate: function (tween)
                {
                    this.sprite.alpha = tween.getValue();
                },
                onUpdateScope: this
            });
        }
        else this.sprite.alpha = state ? 1 : 0;
    }
}
