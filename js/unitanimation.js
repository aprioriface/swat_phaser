class UnitAnimation {
    constructor(unit) {
        this.target = unit;
        this.clear();
    }

    get complete(){
        return this.frames.length === 0 || this.counter >= this.frames.length;
    }

    /**
     * функция очистки массива анимаций
     */
    clear() {
        this.counter = -1;
        this.frames = [];
    }

    addFrame(data){
        this.frames.push(new UnitAnimationFrame(this.target, data));
    }

    nextFrame(){
        this.counter++;
        if (!this.complete){
            const nextFrame = this.frames[this.counter];
            this.target.resprite(nextFrame.state, nextFrame.direction);
            if (nextFrame.moving){
                playground.scene.tweens.add({
                    targets: this.target.sprite,
                    x: nextFrame.coords.x,
                    y: nextFrame.coords.y,
                    onStart: () => {
                        nextFrame.start();
                    },
                    onComplete: () => {
                        nextFrame.complete();
                        this.nextFrame();
                    },
                    duration: nextFrame.duration,
                    ease: 'Linear'
                });
            }
            else {
                if (nextFrame.complete) nextFrame.complete();
                setTimeout(() => {this.nextFrame() }, nextFrame.duration);
            }
        }
        else this.clear();
    }

    /**
     * функция подготовки массива анимаций к прорисовке
     * вставляет кадры поворота юнита и приседания в те места,
     * где это необходимо
     */
    animate() {
        let lastFrame = this.target;
        for (let i = 0; i < this.frames.length; i++) {
            const f = this.frames[i];
            const lowStates = ["sit", "shoot"];
            if (!(lowStates.find(x => x === lastFrame.state) && lowStates.find(x => x === f.state))
                || lowStates.find(x => x === lastFrame.state) && !(lowStates.find(x => x === f.state)))
                this.frames.splice(
                    i++,
                    0,
                    new UnitAnimationStep(this.target, {
                        state: (lastFrame.state === "sit" || lastFrame.state === "shoot") ? "toidle" : "tosit",
                        x: lastFrame.x,
                        y: lastFrame.y,
                        speed: 1
                    })
                );
            lastFrame = f;
        }
        this.nextFrame();
    }
}



class UnitAnimationFrame {
    constructor(unit, {state, x, y, moving, speed}) {
        this.unit = unit;
        this.state = state;
        this.x = x;
        this.y = y;
        this.duration = speed ? 1000 / speed : 0;
        this.moving = moving ? true : false;
        if (moving) {
            this.coords = {
                x: 0,
                y: -10,
            };
        }
        this.straight = ["sit", "shoot", "tosit"].find(x => x === state) ? true : false;
        this.start = () => {
            if (moving) {
                this.unit.replace(x, y, this.direction);
                this.unit.revision({ x: x, y: y, direction: this.direction, straight: this.straight });
            }
        };
        this.complete = () => {
            if (!moving) this.unit.revision({ direction: this.direction, straight: this.straight });
        };
    }

    /**
     * функция, возвращающая направление взгляда юнита в кадре
     * в зависимости от клетки текущего положения и клетки назначения
     * @param {object} curCell - данные клетки текущего положения (x, y, direction)
     * @param {number} x - координата клетки назначения
     * @param {number} y - координата клетки назначения
     * @returns {string}
     */
    get direction() {
        if (this.unit.x === this.x)
            return this.unit.y === this.y ? this.unit.direction : this.unit.y < this.y ? "bl" : "tr";
        else if (this.unit.x < this.x)
            return this.unit.y === this.y ? "br" : this.unit.y < this.y ? "br" : "tr";
        else return this.unit.y === this.y ? "tl" : this.unit.y < this.y ? "bl" : "tl";
    }
}
